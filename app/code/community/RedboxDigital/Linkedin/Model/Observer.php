<?php

class RedboxDigital_Linkedin_Model_Observer
{
    public function changeRequiredField()
    {
        $attribute = Mage::getSingleton("eav/config")->getAttribute("customer", RedboxDigital_Linkedin_Helper_Data::LINKEDIN_ATTRIBUTE_NAME);
        if (Mage::helper('redboxdigital_linkedin')->getLinkedinRequired()) {
            $attribute->setIsRequired(true);
        } else {
            $attribute->setIsRequired(false);
        }
        return $attribute->save();
    }
}