<?php
require_once  Mage::getModuleDir('controllers', 'Mage_Customer').DS.'AccountController.php';

class RedboxDigital_Linkedin_AccountController extends Mage_Customer_AccountController
{
    /**
     * Validate customer data and return errors if they are
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return array|string
     */
    protected function _getCustomerErrors($customer)
    {
        $errors = array();
        $request = $this->getRequest();
        if ($request->getPost('create_address')) {
            $errors = $this->_getErrorsOnCustomerAddress($customer);
        }
        $customerForm = $this->_getCustomerForm($customer);
        $customerData = $customerForm->extractData($request);
        $customerErrors = $customerForm->validateData($customerData);
        $_helper = Mage::helper('redboxdigital_linkedin');
        if($_helper->isEnabled() && $_helper->getLinkedinRequired()){
            if(!Mage::getModel('core/url_validator')->isValid($request->getPost('linkedin_profile'))){
                $errors[] = $this->__('Please enter a valid URL. Protocol is required (http://, https:// or ftp://)');
            }
        }

        if ($customerErrors !== true) {
            $errors = array_merge($customerErrors, $errors);
        } else {
            $customerForm->compactData($customerData);
            $customer->setPassword($request->getPost('password'));
            $customer->setPasswordConfirmation($request->getPost('confirmation'));
            $customerErrors = $customer->validate();
            if (is_array($customerErrors)) {
                $errors = array_merge($customerErrors, $errors);
            }
        }
        return $errors;
    }

}