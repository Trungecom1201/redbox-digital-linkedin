<?php

class RedboxDigital_Linkedin_Test_Model_Observer extends EcomDev_PHPUnit_Test_Case{

    protected $someclass;

    public function setUp()
    {
        $this->someclass = Mage::getModel('redboxdigital_linkedin/observer');
    }

    public function testSomeClass()
    {
        $this->assertInstanceOf('RedboxDigital_Linkedin_Model_Observer',$this->someclass);
    }

    /**
     * Checking exist attribute
     */
    public function testProtectedMethod()
    {
        $r = new ReflectionClass('RedboxDigital_Linkedin_Model_Observer');
        $m = $r->getMethod('changeRequiredField');
        $m->setAccessible(true);
        $result = $m->invoke($this->someclass);
        $this->assertEquals(RedboxDigital_Linkedin_Helper_Data::LINKEDIN_ATTRIBUTE_NAME,$result->getAttributeCode());
    }
    
}