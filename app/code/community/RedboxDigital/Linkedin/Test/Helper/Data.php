<?php

class RedboxDigital_Linkedin_Test_Helper_Data extends EcomDev_PHPUnit_Test_Case_Config
{
    /**
     * Check if the helper aliases are returning the correct class names
     */
    public function testHelperAliases()
    {
        $this->assertHelperAlias('redboxdigital_linkedin', 'RedboxDigital_Linkedin_Helper_Data');
    }

    /**
     * Check default value
     */
    public function testConfigEnabled()
    {
        $this->assertDefaultConfigValue(
            'customer/redboxdigital_linkedin/enable', 1
        );
    }


    /**
     * Check default value
     */
    public function testConfigRequired()
    {
        $this->assertDefaultConfigValue(
            'customer/redboxdigital_linkedin/required', 1
        );
    }
}
