<?php

class RedboxDigital_Linkedin_Test_Config_Main extends EcomDev_PHPUnit_Test_Case_Config
{
    /**
     * Check it the installed module has the correct module version
     */
    public function testModuleConfig()
    {
        $this->assertModuleVersion($this->expected('module')->getVersion());
        $this->assertModuleCodePool($this->expected('module')->getCodePool());

        foreach ($this->expected('module')->getDepends() as $depend) {
            $this->assertModuleIsActive('', $depend);
            $this->assertModuleDepends($depend);
        }
    }
    
}
