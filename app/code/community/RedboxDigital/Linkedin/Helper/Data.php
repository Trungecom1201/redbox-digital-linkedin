<?php

class RedboxDigital_Linkedin_Helper_Data extends Mage_Core_Helper_Data
{
    const LINKEDIN_ATTRIBUTE_NAME = 'linkedin_profile';
    const PATH_LINKEDIN_ENABLED = 'customer/redboxdigital_linkedin/enable';
    const PATH_LINKEDIN_REQUIRED_CLASS = 'customer/redboxdigital_linkedin/required';

    /**
     * @return mixed
     * Display attribute in frontend
     */
    public function isEnabled()
    {
        return Mage::getStoreConfig(self::PATH_LINKEDIN_ENABLED, Mage::app()->getStore()->getId());
    }

    /**
     * @return mixed
     * get class required field
     */
    public function getLinkedinRequired()
    {
        return Mage::getStoreConfig(self::PATH_LINKEDIN_REQUIRED_CLASS, Mage::app()->getStore()->getId());
    }
}